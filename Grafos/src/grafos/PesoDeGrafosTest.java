package grafos;

import static org.junit.Assert.*;

import org.junit.Test;

public class PesoDeGrafosTest {

	@Test
	public void obtenerPesoTest() {
		GrafoConPeso g = completo();
		
		int peso = g.obtenerPeso(0, 2);
		assertTrue(peso == 300);
	}
	
	@Test
	public void obtenerPesolimiteTest() {
		GrafoConPeso g = completo();
		
		int peso = g.obtenerPeso(9, 10);
		assertTrue(peso == 200);
	}
	
	@Test
	public void cambiarPesoTest() {
		GrafoConPeso g = completo();
		g.cambiarPeso(9, 10, 69);
		int peso = g.obtenerPeso(9, 10);
		assertTrue(peso == 69);
	}
	
	private GrafoConPeso completo() {
		GrafoConPeso g = new GrafoConPeso(11);
		g.agregarArista(0, 1, 450); // Bs. As. - Olavarria
		g.agregarArista(0, 2, 300); // Bs. As. - Rosario
		g.agregarArista(0, 3, 30);  // Bs. As. - Ezeiza
		g.agregarArista(2, 3, 330); // Ezeiza - Rosario
		g.agregarArista(5, 3, 650); // Henderson - Rosario
		g.agregarArista(5, 4, 750); // Henderson - Almafuerte
		g.agregarArista(4, 2, 420); // Almafuerte - Rosario
		g.agregarArista(4, 6, 920); // Almafuerte - Puelches
		g.agregarArista(3, 5, 450); // Ezeiza - Henderson
		g.agregarArista(0, 5, 420); // Bs. As. - Henderson
		g.agregarArista(1, 5, 350); // Olavarria - Henderson
		g.agregarArista(1, 6, 670); // Olavarria - Puelches
		g.agregarArista(5, 6, 720); // Henderson - Puelches
		g.agregarArista(1, 7, 250); // Olavarria - Bahia Blanca
		g.agregarArista(5, 7, 420); // Henderson - Bahia Blanca
		g.agregarArista(7, 6, 580); // Bahia Blanca - Puelches
		g.agregarArista(8, 6, 720); // Choele Choel - Puelches
		g.agregarArista(9, 6, 820); // P. Banderita - Puelches
		g.agregarArista(8, 9, 450); // Choele Choel - P. Banderita
		g.agregarArista(9, 10, 200); // P. Banderita - Chocon
		g.agregarArista(8, 10, 640); // Choele Choel - Chocon
		g.agregarArista(10, 6, 480); // Chocon - Puelches
		return g;
		}
	}
