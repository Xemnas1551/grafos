package grafos;

public class Arista{
	
	int entrada;
	int salida;
	int peso;
	
	public Arista(int fuente, int destino, int peso) {
		this.entrada = fuente;
		this.salida = destino;
		this.peso = peso;
		
	}

}
