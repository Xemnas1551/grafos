package agm;

import java.util.ArrayList;

public class Prim2 {
	public static Grafo2  AplicarPrim(Grafo2 m) {
		Grafo2 p = new Grafo2(m.tamanio());
		
		ArrayList<Integer> vertices= new ArrayList<Integer>();
		vertices.add(0);
		
		int i = 0;
		while (i < m.tamanio()-1) {
			Tupla<Integer,Integer> VerticesConAristaMenor= elegirAristaMenorDeVecinos(m,vertices);
			double PesoAristaElegida= m.pesoArista(VerticesConAristaMenor.getX(), VerticesConAristaMenor.getY());
			vertices.add(VerticesConAristaMenor.getY());
			p.agregarArista(VerticesConAristaMenor.getX(), VerticesConAristaMenor.getY(), PesoAristaElegida);
			i=i+1;
		
			System.out.println(vertices.toString());
			
		}
		
		
		
		return p;
			
	}



	private static Tupla<Integer, Integer> elegirAristaMenorDeVecinos(Grafo2 m, ArrayList<Integer> n) {
		Tupla<Integer,Integer> Vertices= new Tupla<Integer, Integer>(0,1);
		double minimo= Integer.MAX_VALUE;
		
		for(int i =0; i < n.size();i++) {
			for(int j: m.vecinos(n.get(i))) {
				
				if(!n.contains(j)) {
					if(m.pesoArista(n.get(i), j)<minimo) {
						Vertices.setX(n.get(i));
						Vertices.setY(j);
						minimo=m.pesoArista(n.get(i), j);
					}
				}
			}
		}
		
		
		return Vertices;
	}
	

}
