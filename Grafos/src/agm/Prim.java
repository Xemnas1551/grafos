package agm;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Prim {
	
	private static Grafo _grafo;
	private static Grafo arbolMinimo;
	
	
	Prim(Grafo grafo){
		_grafo = grafo;
	}
	
	
	public static Grafo ArbolGeneradorMinimo() {
		arbolMinimo = new Grafo(_grafo.tamanio());
		Set<Integer> visitados = new HashSet<>();
		visitados.add(0);
		
		
		int i = 0;
		
		while (i < _grafo.tamanio()-1) {
			int menor = menorVisitados(visitados);
			if(!(menor==Integer.MAX_VALUE)) {
			   visitados.add(menor);
			   i++;	
			}
		}
		return arbolMinimo;	
	}
	
	private static int menorVisitados(Set<Integer> seguros) {
		int menor = Integer.MAX_VALUE;
		int fuente = Integer.MAX_VALUE;
		double peso = Double.MAX_VALUE;
		
		Set<Integer> visitados = seguros;
		for (Integer visitar : visitados) {
			
			Set<Integer> vecinosDeLosVisitados = vecinos(visitar);
			for (Integer posibleSiguiente : vecinosDeLosVisitados) {
				if(!visitados.contains(posibleSiguiente) && peso(visitar,posibleSiguiente) < peso ) {
					fuente = visitar;
					menor = posibleSiguiente;
					peso = peso(visitar,posibleSiguiente);
				}
				
			}
			
		}
		arbolMinimo.agregarArista(fuente, menor, peso);
		return menor;
	}


	private static double peso(Integer arista, Integer vecino) {
		return _grafo.obtenerArista(arista, vecino).peso;
	}


	private static Set<Integer> vecinos(Integer visto) {
		return _grafo.vecinos(visto);
	}
	}
		
	
	
	
	


