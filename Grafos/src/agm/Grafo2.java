package agm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Grafo2 {
	
	private ArrayList< HashMap<Integer, Double>> vecinos; 
	private int tamanio;
	
	public Grafo2(int vertices) {
		this.vecinos = new ArrayList< HashMap<Integer, Double>>();
		this.tamanio = vertices;
		inicializar();
	}
	
	public void inicializar() {
		for(int i = 0; i < tamanio(); i++)
			vecinos.add(new HashMap<Integer, Double>());
	}
	
	public int tamanio() {
		return this.tamanio;
	}
	
	public void agregarArista(int i, int j, double peso) {
		if(i == j)
			throw new IllegalArgumentException("No se permiten loops");
		
		if(i<0 || i>= tamanio())
			throw new IllegalArgumentException("Vertice invalido " + i);
		
		if(j<0 || j>= tamanio())
			throw new IllegalArgumentException("Vertice invalido " + j);
		
		
		vecinos.get(i).put(j, peso);
		vecinos.get(j).put(i, peso);
	}
	
	public boolean existeArista(int i, int j) {
		return vecinos.get(i).containsKey(j);
	}
	
	public void eliminarArista(int i, int j) {
		if(i == j)
			throw new IllegalArgumentException("No es posible eliminar esta arista");
		
		if(!existeArista(i, j))
			throw new IllegalArgumentException("No existe la arista");
		
		vecinos.get(i).remove(j);
	}
	
	public Set<Integer> vecinos(int i) {
		return vecinos.get(i).keySet();
	}
	
	public double pesoArista(int i, int j) {
		return vecinos.get(i).get(j);
	}


	
	
	
	
	public static void main(String[] args) {
		Grafo2 g = new Grafo2(9);
		g.agregarArista(0, 1, 4);
		g.agregarArista(0, 7, 8);
		g.agregarArista(1, 2, 8);
		g.agregarArista(2, 3, 6);
		g.agregarArista(3, 4, 9);
		g.agregarArista(4, 5, 10);
		g.agregarArista(5, 6, 3);
		g.agregarArista(6, 7, 1);
		g.agregarArista(7, 8, 6);
		g.agregarArista(2, 8, 3);
		g.agregarArista(2, 5, 4);
		g.agregarArista(3, 5, 13);
		g.agregarArista(1, 7, 12);
		g.agregarArista(6, 8, 5);
		
		Grafo2 g2 = Prim2.AplicarPrim(g);
		String[] nodos = {"a","b","c","d","e","f","g","h","i",}; 
		for (int i = 0; i <g2.tamanio(); i++) {
			System.out.println(g2.vecinos(i) + "nodo: "+ nodos[i]);
			
		}

}}
