package agm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class Grafo {
	
	private ArrayList<HashMap<Integer,Arista>> vecinos; 
	private int tamanio;
	
	public Grafo(int vertices) {
		this.vecinos = new ArrayList();
		this.tamanio = vertices;
		inicializar();
	}
	
	public void inicializar() {
		for(int i = 0; i < tamanio(); i++)
			vecinos.add(i, new HashMap<Integer,Arista>()); 
	}
	
	public int tamanio() {
		return this.tamanio;
	}
	
	public void agregarArista(int i, int j, double peso) {
		if(i == j)
			throw new IllegalArgumentException("No se permiten loops");
		
		if(i<0 || i>= tamanio())
			throw new IllegalArgumentException("Vertice invalido " + i);
		
		if(j<0 || j>= tamanio())
			throw new IllegalArgumentException("Vertice invalido " + j);
		
		Arista arista = new Arista(i,j,peso);
		Arista arista2 = new Arista(j,i,peso);
		vecinos.get(i).put(j, arista);
		vecinos.get(j).put(i, arista2);
	}
	
	public boolean existeArista(int i, int j) {
		return vecinos.get(i).containsKey(j);
		
	}
	
	public void eliminarArista(int i, int j) {
		if(i == j)
			throw new IllegalArgumentException("No es posible eliminar esta arista");
		
		if(!existeArista(i, j))
			throw new IllegalArgumentException("No existe la arista");
		
		
	}
	
	public Set<Integer> vecinos(int i) {
		return vecinos.get(i).keySet();
	}
	
	public double pesoArista(int i, int j) {
		Arista arista = vecinos.get(i).get(j);
		return arista.peso;
	}
	public Arista obtenerArista(int i, int j) {
		return vecinos.get(i).get(j);
	}
	public static void main(String[] args) {
		Grafo g = new Grafo(9);
		g.agregarArista(0, 1, 4);
		g.agregarArista(0, 7, 8);
		g.agregarArista(1, 2, 8);
		g.agregarArista(2, 3, 6);
		g.agregarArista(3, 4, 9);
		g.agregarArista(4, 5, 10);
		g.agregarArista(5, 6, 3);
		g.agregarArista(6, 7, 1);
		g.agregarArista(7, 8, 6);
		g.agregarArista(2, 8, 3);
		g.agregarArista(2, 5, 4);
		g.agregarArista(3, 5, 13);
		g.agregarArista(1, 7, 12);
		g.agregarArista(6, 8, 5);
		
		Grafo g2 = new Prim(g).ArbolGeneradorMinimo();
		for (int i = 0; i <g2.tamanio; i++) {
			System.out.println(g2.vecinos(i) + "nodo: "+ i);
			
		}
		
	}}

