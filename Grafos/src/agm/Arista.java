package agm;

public class Arista {
	int fuente;
	int destino;
	double peso;
	
	Arista(int fuente, int destino, double peso){
		this.fuente= fuente;
		this.destino= destino;
		this.peso=peso;
		
	}

	public int getFuente() {
		return fuente;
	}

	public void setFuente(int fuente) {
		this.fuente = fuente;
	}

	public int getDestino() {
		return destino;
	}

	public void setDestino(int destino) {
		this.destino = destino;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	
	

}
