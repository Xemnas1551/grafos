package funciones;

import java.util.Set;

import grafos.Grafo;

public class Funciones {
	//usando el coeficiente binomial se puede saber cuantos triangulos tiene cualquier grafo clique
	public static Integer cantidadDetriangulos(Grafo grafo) {
		if(grafo.tamano()<3) 
			return 0;
		
		Integer triangulos = 0;
		for (int i = 0; i < grafo.tamano(); i++) {
			for (int j = 0; j < grafo.tamano(); j++) {
				if( i!=j && grafo.existeArista(i, j)) {
					for (int k = 0; k < grafo.tamano(); k++ ){
						if( i!=k && j!=k && grafo.existeArista(k, i)&&grafo.existeArista(k, j)) { 
							triangulos++;	
						}
					}
				}
				
			}
			
		}
		return triangulos/6;
	}
	
	public static boolean estanADosDeDistancia(Grafo grafo, int vertice1 ,int vertice2 ) {
		if(grafo.existeArista(vertice1, vertice2)) {
			return false;
		}
		if (grafo.tamano()<3) {
			return false;
		}
		Set<Integer> vecinos1 = grafo.vecinos(vertice1);
		Set<Integer> vecinos2 = grafo.vecinos(vertice2);
		
		for (Integer vecino:vecinos1) {
				if (vecinos2.contains(vecino)) {
					return true;
				}
			}
		return false;
		
		}
//si se utiliza una matriz de incidencia obtener los vecinos de un nodoe es O(1)
	public static boolean verticeUniversal(Grafo grafo, int vertice) {
		int vecinos = grafo.vecinos(vertice).size();
		if((vecinos+1)==grafo.tamano()) {
			return true;
		}
		return false;
	}
	
	
	public static void main(String[] args) {
		

		Grafo grafo = new Grafo(5);
		grafo.agregarArista(4, 0);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(3, 2);
			
			
		
		
		System.out.println(estanADosDeDistancia(grafo, 0, 3));
	}
	

	

}
