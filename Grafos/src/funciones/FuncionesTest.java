package funciones;

import static org.junit.Assert.*;

import org.junit.Test;

import grafos.Assert;
import grafos.Grafo;

public class FuncionesTest {

	@Test
	public void unicoTriangulotest() {
		Integer totalInteger = Funciones.cantidadDetriangulos(trianguloConAntena());
		assertTrue(1 == totalInteger);
	}
	
	@Test
	public void muchosTriangulotest() {
		Integer total = Funciones.cantidadDetriangulos(completo());
		Integer esperado = 10;
		assertEquals(esperado, total);
	}
	
	
	@Test 
	public void ADosDeDistanciaTest() {
		Grafo grafo = trianguloConAntena();
		
		assertTrue(Funciones.estanADosDeDistancia(grafo, 0, 3));
		
		
		
	}
	
	@Test
	public void noEstanADosDeDistanciaTest() {
		Grafo grafo = aMasDeDosDistancia();
		assertFalse(Funciones.estanADosDeDistancia(grafo, 3, 0));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void esElMismoTest() {
		Grafo grafo = aMasDeDosDistancia();
		Funciones.estanADosDeDistancia(grafo, 0, 0);
		
	}
	@Test
	public void esUniversarTest() {
		Grafo grafo = ceroUniversal();
		
		assertTrue(Funciones.verticeUniversal(grafo, 0));
	}
	
	@Test
	public void noEsUniversarTest() {
		Grafo grafo = ceroUniversal();
		
		assertFalse(Funciones.verticeUniversal(grafo, 3));
	}
	
	private Grafo trianguloConAntena()
	{
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(3, 1);
		
		return grafo;
	}
	
	private Grafo ceroUniversal()
	{
		Grafo grafo = new Grafo(4);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(1, 2);
		
		
		return grafo;
	}
	private Grafo completo() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(0, 2);
		grafo.agregarArista(0, 3);
		grafo.agregarArista(0, 4);
		grafo.agregarArista(1,2);
		grafo.agregarArista(1,3);
		grafo.agregarArista(1,4);
		grafo.agregarArista(2,3);
		grafo.agregarArista(2,4);
		grafo.agregarArista(3,4);
		
		return grafo;
	}
	//3 y 0 estan a mas de dos vertices de distancia
	private Grafo aMasDeDosDistancia() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(4, 0);
		grafo.agregarArista(0, 1);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(3, 2);
		return grafo;
	}
}
