package funciones;

import grafos.Grafo;
import grafos.GrafoConPeso;

public abstract class Algoritmos {
	
	public abstract GrafoConPeso prim(GrafoConPeso grafo);

}
